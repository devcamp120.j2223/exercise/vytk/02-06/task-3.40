// Lệnh này tương tự import express from 'express' // Dùng để import thư viện express vào project
const express = require('express');
const { drinkClassList, drinkObjectList } = require("./drink"); // Tương tự: import {data} from "./data";
// Khởi tạo app express
const app=express();

//Cấu hình app đọc được body request JSON
app.use(express.json());
//Cấu hình app đọc được tieng Viet uft8
app.use(express.urlencoded({
    extended:true
}));
// Khai báo cổng project
const port=8000;

// Khai báo API dạng get '/'
// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    response.status(200).json({
        message: "Drinks API"
    })
})
//Call back function: Là một tham số của hàm khác và nó sẽ được thực thi sau khi hàm đó được call

app.get('/drink-class',(request,response) => {
    let code=request.query.code;
    if(code){
        let drinkListRes=[];
        for(let i=0;i<drinkClassList.length;i++){
            if(drinkClassList[i].checkDrinkByCode(code)) {
                drinkListRes.push(drinkClassList[i]);
            }
        };
        response.status(200).json({
            listDrink:drinkListRes
        });
    }else{
        response.status(200).json({
            listDrink:drinkClassList
        });
    }
})

app.get('/drink-object',(request,response) => {
    let code=request.query.code;
    if(code){
        let drinkListRes=[];
        for(let i=0;i<drinkObjectList.length;i++){
            if(drinkObjectList[i].checkDrinkByCode(code)) {
                drinkListRes.push(drinkObjectList[i]);
            }
        };
        response.status(200).json({
            Drink:drinkListRes
        });
    }else{
        response.status(200).json({
            Drink:drinkObjectList
        });
    }
})

app.get('/drink-class/:drinkId',(request,response)=>{
    let drinkId = request.params.drinkId;
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
    drinkId = parseInt(drinkId); 

    // Khởi tạo biến drinkResponse là kết quả tìm được 
    let drinkResponse = null;

    // Duyệt từng phần tử của mảng để tìm được đồ uống có Id tương ứng
    for (let index = 0; index < drinkClassList.length; index++) {
        if(drinkClassList[index].checkDrinkById(drinkId)) {
            drinkResponse = drinkClassList[index];
            // Nếu tìm được thì thoát khỏi vòng lặp ngay lập tức
            break;
        }
    };

    response.status(200).json({
        drinks: drinkResponse
    })
});
app.get('/drink-object/:drinkId',(request,response)=>{
    let drinkId = request.params.drinkId;
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
    drinkId = parseInt(drinkId); 

    // Khởi tạo biến drinkResponse là kết quả tìm được 
    let drinkResponse = null;

    // Duyệt từng phần tử của mảng để tìm được đồ uống có Id tương ứng
    for (let index = 0; index < drinkClassList.length; index++) {
        if(drinkClassList[index].id === drinkId) {
            drinkResponse = drinkClassList[index];
            // Nếu tìm được thì thoát khỏi vòng lặp ngay lập tức
            break;
        }
    };

    response.status(200).json({
        drinks: drinkResponse
    })
});
//Chạy app express
app.listen(port,()=>{
    console.log(`App listening on port: ${port}`);
})
