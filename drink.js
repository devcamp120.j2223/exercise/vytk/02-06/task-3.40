class Drink{
    constructor(paramid,paramidDrink,paramname,paramprice,paramdateCreate,paramdateUpdate){
        this.id=paramid;
        this.idDrink=paramidDrink;
        this.name=paramname;
        this.price=paramprice;
        this.dateCreate=paramdateCreate;
        this.dateUpdate=paramdateUpdate;
    }
    // Hàm kiểm tra đồ uống có chứa ID được truyền vào hay không
    checkDrinkById(paramid) {
        return this.id === paramid
    }

    // Hàm kiểm tra đồ uống có code chứa mã được truyền vào hay không
    checkDrinkByCode(paramCode) {
        return this.code.includes(paramCode.toUpperCase());
    }
}
let drinkClassList = [];
var traTac=new Drink(1,"TRATAC","Trà Tắc",10000,"14/5/2021","14/5/2021");
drinkClassList.push(traTac);
var coCa=new Drink(2,"COCA","Cocacola",15000,"14/5/2021","14/5/2021");
drinkClassList.push(coCa);
var pepSi=new Drink(3,"PEPSI","Pepsi",15000,"14/5/2021","14/5/2021");
drinkClassList.push(pepSi);

let drinkObjectList = [
    {
        id: 1,
        code: "TRATAC",
        name: "Trà tắc",
        price: 10000,
        createAt: "14/5/2021",
        updateAt: "14/5/2021"
    },
    {
        id: 2,
        code: "COCA",
        name: "Cocacola",
        price: 15000,
        createAt: "14/5/2021",
        updateAt: "14/5/2021"
    },
    {
        id: 3,
        code: "PEPSI",
        name: "Pepsi",
        price: 15000,
        createAt: "14/5/2021",
        updateAt: "14/5/2021"
    }
]

module.exports = {
    drinkClassList,
    drinkObjectList
}